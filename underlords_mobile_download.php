<?php
declare(strict_types=1);

// Alternatively manifest_ios
$manifest = file_get_contents( 'https://raw.githubusercontent.com/SteamDatabase/GameTracking-Underlords-Android/master/manifest_android.json' );
$manifest = json_decode( $manifest, true );

$out = "#!/bin/bash\n";

foreach( $manifest[ 'assets' ] as $assetName => $asset )
{
	if( strpos( $assetName, 'pak01' ) === false )
	{
		continue;
	}

	$out .= "echo '=========== Downloading {$assetName}'\n";
	$out .= 'curl --create-dirs -o ' . escapeshellarg( $assetName ) . ' ' . escapeshellarg( $manifest[ 'cdnroot' ] . $assetName ) . "\n";
}

file_put_contents( 'download_underlords.sh', $out );

echo 'bash ./download_underlords.sh' . PHP_EOL;
